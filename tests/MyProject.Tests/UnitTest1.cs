using MyProject.Application;

namespace MyProject.Tests;

public class UnitTest1
{
    [Fact]
    public void Test1()
    {
        // Arrange
        var class1 = new Class1();
        int expected = 3;

        // Act
        int actual = class1.Add(1, 2);

        // Assert
        Assert.Equal(expected, actual);
    }
}
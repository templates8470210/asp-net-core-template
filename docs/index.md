# Home

Welcome to this project's documentation. Here's how I managed to build this project, and how I use it. 

## Building the folder structure

The whole structure was created using `dotnet-cli`. 

```powershell
# creating folder structure 
mkdir src
mkdir tests
mkdir docs
# create sln and csproj
dotnet new sln --name MyProject
cd .\src
dotnet new mvc --name MyProject.Web
dotnet new classlib --name MyProject.Application
cd ..\tests
dotnet new xunit --name MyProject.Tests
# add projects to sln in solution folders
cd ..
dotnet sln .\MyProject.sln add .\src\MyProject.Application\MyProject.Application.csproj --solution-folder src
dotnet sln .\MyProject.sln add .\src\MyProject.Web\MyProject.Web.csproj --solution-folder src
dotnet sln .\MyProject.sln add .\tests\MyProject.Tests\MyProject.Tests.csproj --solution-folder tests
# add project references
cd .\src\MyProject.Web
dotnet add reference ..\MyProject.Application\MyProject.Application.csproj
cd ..\..\tests\MyProject.Tests
dotnet add reference ..\..\src\MyProject.Web\MyProject.Web.csproj
dotnet add reference ..\..\src\MyProject.Application\MyProject.Application.csproj
```

## Badges

Once this project is setup, you might want to add some badges for this Gitlab project.

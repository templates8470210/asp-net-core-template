# asp-net-core-template

The main goal of this project is to show *one* way of working with **AspNetCore** and **Gitlab**, for the full life-cycle.

## Pre-requisites 

- Docfx v2.63.1
- .net7

## Documentation

Please find some getting-started guide and documentations [here](./docs/articles/getting-started.md).

## TODO

- [ ] Template this project in another repo, to just use `dotnet new asp-net-core-template`
